using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using ArchiSteamFarm;
using Newtonsoft.Json.Linq;

namespace Vital7.SummerEvent {
	internal class BotEventInstance : IDisposable {
		internal BotEventInstance(Bot bot) {
			Bot = bot;
			Commands = new PluginCommands(this);
			Handler = new AchievementHandler(bot);
			WebHandler = new WebHandler(bot);
		}

		internal bool AutoUpgradeEventBadge { get; private set; }
		internal bool DisableAutoSummerEvent { get; set; }

		internal Bot Bot { get; }
		internal AchievementHandler Handler { get; }
		internal PluginCommands Commands { get; }
		internal WebHandler WebHandler { get; }

		internal Timer EventTimer { get; set; }

		public void Dispose() {
			EventTimer?.Dispose();
		}

		internal async Task<string> OnBotCommand(ulong steamID, string message, string[] args) => await Commands.OnBotCommand(steamID, message, args).ConfigureAwait(false);

		internal void OnBotDisconnected() {
			Dispose();
		}

		internal void OnBotInitModules(IReadOnlyDictionary<string, JToken> additionalConfigProperties) {
			AutoUpgradeEventBadge = additionalConfigProperties.TryGetValue("AutoUpgradeEventBadge", out JToken autoUpgradeEventBadgeJsonToken) && (autoUpgradeEventBadgeJsonToken.Type == JTokenType.Boolean) && autoUpgradeEventBadgeJsonToken.ToObject<bool>();
			DisableAutoSummerEvent = additionalConfigProperties.TryGetValue("DisableAutoSummerEvent", out JToken disableSummerEventJsonToken) && (disableSummerEventJsonToken.Type == JTokenType.Boolean) && disableSummerEventJsonToken.ToObject<bool>();
		}

		internal void OnBotLoggedOn() {
			if (DisableAutoSummerEvent || (SummerEvent.SummerEventEnd < DateTime.UtcNow)) {
				return;
			}

			EventTimer = new Timer(
				async e => await Commands.ResponseEvent(ASF.GlobalConfig.SteamOwnerID).ConfigureAwait(false),
				null,
				TimeSpan.FromMinutes(1),
				TimeSpan.FromHours(5.9));
		}
	}
}
