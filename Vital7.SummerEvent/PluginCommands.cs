using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using ArchiSteamFarm;
using ArchiSteamFarm.Json;
using ArchiSteamFarm.Localization;

namespace Vital7.SummerEvent {
	internal class PluginCommands {
		private readonly BotEventInstance BotEvent;
		internal PluginCommands(BotEventInstance bot) => BotEvent = bot;

		internal async Task<string> OnBotCommand(ulong steamID, string message, string[] args) {
			if ((steamID == 0) || string.IsNullOrEmpty(message)) {
				return null;
			}

			switch (args[0].ToUpperInvariant()) {
				case "CONVERT" when args.Length > 1:
					return await ResponseConvert(steamID, ArchiSteamFarm.Utilities.GetArgsAsText(args, 1, ",")).ConfigureAwait(false);
				case "CONVERT":
					return await ResponseConvert(steamID).ConfigureAwait(false);
				case "EVENT" when args.Length > 1:
					return await ResponseEvent(steamID, ArchiSteamFarm.Utilities.GetArgsAsText(args, 1, ",")).ConfigureAwait(false);
				case "EVENT":
					return await ResponseEvent(steamID).ConfigureAwait(false);
				case "GETINFO" when args.Length > 1:
					return await ResponseGetInfo(steamID, ArchiSteamFarm.Utilities.GetArgsAsText(args, 1, ",")).ConfigureAwait(false);
				case "GETINFO":
					return await ResponseGetInfo(steamID).ConfigureAwait(false);
				case "GETLEVEL" when args.Length > 1:
					return await ResponseGetLevel(steamID, ArchiSteamFarm.Utilities.GetArgsAsText(args, 1, ",")).ConfigureAwait(false);
				case "GETLEVEL":
					return await ResponseGetLevel(steamID).ConfigureAwait(false);
				case "GETTOKENS" when args.Length > 1:
					return await ResponseGetTokens(steamID, ArchiSteamFarm.Utilities.GetArgsAsText(args, 1, ",")).ConfigureAwait(false);
				case "GETTOKENS":
					return await ResponseGetTokens(steamID).ConfigureAwait(false);
				case "LOCKALL" when args.Length > 2:
					return await ResponseLockAll(steamID, args[1], args[2]).ConfigureAwait(false);
				case "LOCKALL" when args.Length > 1:
					return await ResponseLockAll(steamID, args[1]).ConfigureAwait(false);
				case "UNLOCKALL" when args.Length > 2:
					return await ResponseUnlockAll(steamID, args[1], args[2]).ConfigureAwait(false);
				case "UNLOCKALL" when args.Length > 1:
					return await ResponseUnlockAll(steamID, args[1]).ConfigureAwait(false);
				case "UPDATEPLUGIN":
					return await ResponseUpdatePlugin(steamID).ConfigureAwait(false);
				case "UPGRADEBADGE" when args.Length > 2:
					return await ResponseUpgradeBadge(steamID, args[1], args[2]).ConfigureAwait(false);
				case "UPGRADEBADGE" when args.Length > 1:
					return await ResponseUpgradeBadge(steamID, args[1]).ConfigureAwait(false);
				case "VERSIONPLUGIN":
					return Commands.FormatStaticResponse(SummerEvent.PluginVersion.ToString());
			}

			return null;
		}

		private async Task<string> ResponseConvert(ulong steamID) {
			if (steamID == 0) {
				BotEvent.Bot.ArchiLogger.LogNullError(nameof(steamID));
				return null;
			}

			if (!BotEvent.Bot.HasPermission(steamID, BotConfig.EPermission.Master)) {
				return null;
			}

			if (!BotEvent.Bot.IsConnectedAndLoggedOn) {
				return BotEvent.Bot.Commands.FormatBotResponse(Strings.BotNotConnected);
			}

			uint points = ((await BotEvent.WebHandler.GetGrandPrixInfo().ConfigureAwait(false))?.Points).GetValueOrDefault(1);
			for (int i = 0; i < (int) Math.Ceiling(points / 1000.0); i++) {
				await BotEvent.Bot.ArchiWebHandler.UrlPostToJsonObjectWithSession<Steam.NumberResponse>("https://store.steampowered.com", "/grandprix/ajaxboostteam/", new Dictionary<string, string>(1, StringComparer.Ordinal)).ConfigureAwait(false);
			}

			return BotEvent.Bot.Commands.FormatBotResponse(Strings.Done);
		}

		private static async Task<string> ResponseConvert(ulong steamID, string botNames) {
			if ((steamID == 0) || string.IsNullOrEmpty(botNames)) {
				ASF.ArchiLogger.LogNullError(nameof(steamID) + " || " + nameof(botNames));
				return null;
			}

			HashSet<Bot> bots = Bot.GetBots(botNames);
			if ((bots == null) || (bots.Count == 0)) {
				return ASF.IsOwner(steamID) ? Commands.FormatStaticResponse(string.Format(Strings.BotNotFound, botNames)) : null;
			}

			IList<string> results = await ArchiSteamFarm.Utilities.InParallel(bots.Select(bot => SummerEvent.BotEventInstances[bot].Commands.ResponseConvert(steamID))).ConfigureAwait(false);

			List<string> responses = new List<string>(results.Where(result => !string.IsNullOrEmpty(result)));
			return responses.Count > 0 ? string.Join(Environment.NewLine, responses) : null;
		}

		internal async Task<string> ResponseEvent(ulong steamID) {
			try {
				if (steamID == 0) {
					BotEvent.Bot.ArchiLogger.LogNullError(nameof(steamID));
					return null;
				}

				if (!BotEvent.Bot.HasPermission(steamID, BotConfig.EPermission.Master)) {
					return null;
				}

				if ((BotEvent.DisableAutoSummerEvent || (SummerEvent.SummerEventEnd < DateTime.UtcNow)) && (BotEvent.EventTimer != null)) {
					BotEvent.EventTimer.Dispose();
					BotEvent.EventTimer = null;
					return BotEvent.Bot.Commands.FormatBotResponse("Disabled");
				}

				if (!BotEvent.Bot.IsConnectedAndLoggedOn) {
					return BotEvent.Bot.Commands.FormatBotResponse(Strings.BotNotConnected);
				}

				WebHandler.GrandPrixPageInfo grandPrixInfo = await BotEvent.WebHandler.GetGrandPrixInfo().ConfigureAwait(false);
				if (grandPrixInfo == null) {
					return BotEvent.Bot.Commands.FormatBotResponse(string.Format(Strings.ErrorObjectIsNull, nameof(grandPrixInfo)));
				}

				if (grandPrixInfo.CurrentTeam.GetValueOrDefault(0) == 0) {
					bool? joinTeam = await BotEvent.WebHandler.JoinTeam((byte) (Utilities.RandomNext(5) + 1)).ConfigureAwait(false);
					if (joinTeam.GetValueOrDefault(false)) {
						BotEvent.Bot.ArchiLogger.LogGenericDebug(string.Format(Strings.WarningFailedWithError, nameof(joinTeam)));
					}
				}

				// ReSharper disable once PossibleInvalidOperationException
				if ((grandPrixInfo.CurrentTeam.GetValueOrDefault(0) != 0) && !await BotEvent.WebHandler.SwitchTeam(grandPrixInfo.CurrentTeam.Value).ConfigureAwait(false)) {
					BotEvent.Bot.ArchiLogger.LogGenericDebug(string.Format(Strings.WarningFailedWithError, nameof(WebHandler.SwitchTeam)));
				}

				bool[] tasks = grandPrixInfo.AvailableTasks;
				if (tasks != null) {
					if (tasks[0]) {
						uint? appID = await BotEvent.WebHandler.GetRandomFreeGame().ConfigureAwait(false);
						if (appID != null) {
							await BotEvent.Bot.Commands.Response(ASF.GlobalConfig.SteamOwnerID, $"addlicense {BotEvent.Bot.BotName} {appID}").ConfigureAwait(false);

							(bool success, _) = await BotEvent.Bot.Actions.Play(new[] {appID.Value}).ConfigureAwait(false);
							if (success) {
								await Task.Delay(3000).ConfigureAwait(false);
							} else {
								BotEvent.Bot.ArchiLogger.LogGenericDebug(string.Format(Strings.WarningFailedWithError, nameof(Actions.Play)));
							}

							BotEvent.Bot.Actions.Resume();
						}
					}

					if (tasks[2]) {
						for (int i = 0; i < 3; i++) {
							uint? appID = await BotEvent.WebHandler.GetRandomGame().ConfigureAwait(false);
							if (appID == null) {
								i--;
								continue;
							}

							if (!(await BotEvent.WebHandler.AddToWishlist(appID.Value).ConfigureAwait(false)).GetValueOrDefault(false)) {
								BotEvent.Bot.ArchiLogger.LogGenericDebug(string.Format(Strings.WarningFailedWithError, nameof(WebHandler.AddToWishlist)));
							}
						}
					}

					if (tasks[3]) {
						if (!await BotEvent.Bot.ArchiWebHandler.DoTaskBroadcast().ConfigureAwait(false)) {
							BotEvent.Bot.ArchiLogger.LogGenericWarning(string.Format(Strings.WarningFailedWithError, nameof(PrivateMembersExtensions.DoTaskBroadcast)));
						}
					}
				}

				HashSet<uint> pastGames = grandPrixInfo.PastGames;
				if ((pastGames != null) && (pastGames.Count > 0)) {
					foreach (uint pastGame in pastGames) {
						if ((!await BotEvent.WebHandler.ClaimPastAchievements(pastGame).ConfigureAwait(false)).GetValueOrDefault(false)) {
							BotEvent.Bot.ArchiLogger.LogGenericDebug(string.Format(Strings.WarningFailedWithError, nameof(WebHandler.ClaimPastAchievements)));
						}
					}
				}

				grandPrixInfo = await BotEvent.WebHandler.GetGrandPrixInfo().ConfigureAwait(false);
				uint points = (grandPrixInfo?.Points).GetValueOrDefault(0);
				uint bankCapacity = (grandPrixInfo?.Capacity).GetValueOrDefault(0);
				if ((bankCapacity != 0) && (points < bankCapacity)) {
					Dictionary<uint, string> games = await BotEvent.Bot.Commands.FetchGamesOwned().ConfigureAwait(false);
					if (games == null) {
						return BotEvent.Bot.Commands.FormatBotResponse(string.Format(Strings.ErrorObjectIsNull, nameof(games)));
					}

					List<uint> gamesOpened = new List<uint>();
					for (int i = 0; i < 3; i++) {
						uint game = 0;
						bool validGame = false;
						while (!validGame && (games.Count > 0)) {
							game = games.GetRandomValue().Key;
							games.Remove(game);
							validGame = (await BotEvent.Bot.ArchiWebHandler.UrlGetToHtmlDocumentWithSession("https://steamcommunity.com", "/my/stats/" + game, false).ConfigureAwait(false))?.DocumentNode.SelectSingleNode("//div[@class='achievements_list ']") != null;
						}

						if (games.Count == 0) {
							return BotEvent.Bot.Commands.FormatBotResponse(string.Format(Strings.ErrorIsEmpty, nameof(games)));
						}

						gamesOpened.Add(game);
						for (int j = 0; j < 3; j++) {
							await Task.Delay(500).ConfigureAwait(false);
							await BotEvent.Handler.AchievementAction(game, false).ConfigureAwait(false);
							await Task.Delay(500).ConfigureAwait(false);
							await BotEvent.Handler.AchievementAction(game, true).ConfigureAwait(false);
						}
					}

					Dictionary<uint, uint> playtime = await BotEvent.WebHandler.GetPlaytime().ConfigureAwait(false);
					byte minutesToWait = 31;
					if ((playtime != null) && gamesOpened.All(game => playtime.ContainsKey(game))) {
						uint minimumPlaytime = playtime.Where(game => gamesOpened.Contains(game.Key)).Min(x => x.Value);
						minutesToWait = (byte) (minimumPlaytime > 30 ? 0 : 31 - minimumPlaytime);
					}

					if (minutesToWait > 0) {
						await BotEvent.Bot.Actions.Play(gamesOpened).ConfigureAwait(false);
						await Task.Delay(minutesToWait * 60 * 1000).ConfigureAwait(false);
						BotEvent.Bot.Actions.Resume();
					}
				}

				points = ((await BotEvent.WebHandler.GetGrandPrixInfo().ConfigureAwait(false))?.Points).GetValueOrDefault(1);
				for (int i = 0; i < (int) Math.Ceiling(points / 1000.0); i++) {
					await BotEvent.Bot.ArchiWebHandler.UrlPostToJsonObjectWithSession<Steam.NumberResponse>("https://store.steampowered.com", "/grandprix/ajaxboostteam/", new Dictionary<string, string>(1, StringComparer.Ordinal)).ConfigureAwait(false);
				}

				if (BotEvent.AutoUpgradeEventBadge) {
					await ResponseUpgradeBadge(steamID, "max").ConfigureAwait(false);
				}

				return BotEvent.Bot.Commands.FormatBotResponse(Strings.Done);
			} catch (Exception e) {
				BotEvent.Bot.ArchiLogger.LogGenericWarningException(e);
				return BotEvent.Bot != null ? BotEvent.Bot.Commands.FormatBotResponse(Strings.WarningFailed) : Commands.FormatStaticResponse(Strings.WarningFailed);
			}
		}

		private static async Task<string> ResponseEvent(ulong steamID, string botNames) {
			if ((steamID == 0) || string.IsNullOrEmpty(botNames)) {
				ASF.ArchiLogger.LogNullError(nameof(steamID) + " || " + nameof(botNames));
				return null;
			}

			HashSet<Bot> bots = Bot.GetBots(botNames);
			if ((bots == null) || (bots.Count == 0)) {
				return ASF.IsOwner(steamID) ? Commands.FormatStaticResponse(string.Format(Strings.BotNotFound, botNames)) : null;
			}

			IList<string> results = await ArchiSteamFarm.Utilities.InParallel(bots.Select(bot => SummerEvent.BotEventInstances[bot].Commands.ResponseEvent(steamID))).ConfigureAwait(false);

			List<string> responses = new List<string>(results.Where(result => !string.IsNullOrEmpty(result)));
			return responses.Count > 0 ? string.Join(Environment.NewLine, responses) : null;
		}

		private async Task<string> ResponseGetInfo(ulong steamID) {
			if (steamID == 0) {
				BotEvent.Bot.ArchiLogger.LogNullError(nameof(steamID));
				return null;
			}

			if (!BotEvent.Bot.HasPermission(steamID, BotConfig.EPermission.Master)) {
				return null;
			}

			if (!BotEvent.Bot.IsConnectedAndLoggedOn) {
				return BotEvent.Bot.Commands.FormatBotResponse(Strings.BotNotConnected);
			}

			WebHandler.GrandPrixPageInfo grandPrixInfo = await BotEvent.WebHandler.GetGrandPrixInfo();
			return BotEvent.Bot.Commands.FormatBotResponse((grandPrixInfo?.Points?.ToString() ?? "null") + "/" + (grandPrixInfo?.Capacity?.ToString() ?? "null"));
		}

		private static async Task<string> ResponseGetInfo(ulong steamID, string botNames) {
			if ((steamID == 0) || string.IsNullOrEmpty(botNames)) {
				ASF.ArchiLogger.LogNullError(nameof(steamID) + " || " + nameof(botNames));
				return null;
			}

			HashSet<Bot> bots = Bot.GetBots(botNames);
			if ((bots == null) || (bots.Count == 0)) {
				return ASF.IsOwner(steamID) ? Commands.FormatStaticResponse(string.Format(Strings.BotNotFound, botNames)) : null;
			}

			IList<string> results = await ArchiSteamFarm.Utilities.InParallel(bots.Select(bot => SummerEvent.BotEventInstances[bot].Commands.ResponseGetInfo(steamID))).ConfigureAwait(false);

			List<string> responses = new List<string>(results.Where(result => !string.IsNullOrEmpty(result)));
			return responses.Count > 0 ? string.Join(Environment.NewLine, responses) : null;
		}

		private async Task<string> ResponseGetLevel(ulong steamID) {
			if (steamID == 0) {
				BotEvent.Bot.ArchiLogger.LogNullError(nameof(steamID));
				return null;
			}

			if (!BotEvent.Bot.HasPermission(steamID, BotConfig.EPermission.Master)) {
				return null;
			}

			if (!BotEvent.Bot.IsConnectedAndLoggedOn) {
				return BotEvent.Bot.Commands.FormatBotResponse(Strings.BotNotConnected);
			}

			ulong? level = await BotEvent.WebHandler.GetLevel().ConfigureAwait(false);
			return BotEvent.Bot.Commands.FormatBotResponse(level == null ? Strings.WarningFailed : level.Value.ToString());
		}

		private static async Task<string> ResponseGetLevel(ulong steamID, string botNames) {
			if ((steamID == 0) || string.IsNullOrEmpty(botNames)) {
				ASF.ArchiLogger.LogNullError(nameof(steamID) + " || " + nameof(botNames));
				return null;
			}

			HashSet<Bot> bots = Bot.GetBots(botNames);
			if ((bots == null) || (bots.Count == 0)) {
				return ASF.IsOwner(steamID) ? Commands.FormatStaticResponse(string.Format(Strings.BotNotFound, botNames)) : null;
			}

			IList<string> results = await ArchiSteamFarm.Utilities.InParallel(bots.Select(bot => SummerEvent.BotEventInstances[bot].Commands.ResponseGetLevel(steamID))).ConfigureAwait(false);

			List<string> responses = new List<string>(results.Where(result => !string.IsNullOrEmpty(result)));
			return responses.Count > 0 ? string.Join(Environment.NewLine, responses) : null;
		}

		private async Task<string> ResponseGetTokens(ulong steamID) {
			if (steamID == 0) {
				BotEvent.Bot.ArchiLogger.LogNullError(nameof(steamID));
				return null;
			}

			if (!BotEvent.Bot.HasPermission(steamID, BotConfig.EPermission.Master)) {
				return null;
			}

			if (!BotEvent.Bot.IsConnectedAndLoggedOn) {
				return BotEvent.Bot.Commands.FormatBotResponse(Strings.BotNotConnected);
			}

			return BotEvent.Bot.Commands.FormatBotResponse((await BotEvent.WebHandler.GetTokens().ConfigureAwait(false)).ToString());
		}

		private static async Task<string> ResponseGetTokens(ulong steamID, string botNames) {
			if ((steamID == 0) || string.IsNullOrEmpty(botNames)) {
				ASF.ArchiLogger.LogNullError(nameof(steamID) + " || " + nameof(botNames));
				return null;
			}

			HashSet<Bot> bots = Bot.GetBots(botNames);
			if ((bots == null) || (bots.Count == 0)) {
				return ASF.IsOwner(steamID) ? Commands.FormatStaticResponse(string.Format(Strings.BotNotFound, botNames)) : null;
			}

			IList<string> results = await ArchiSteamFarm.Utilities.InParallel(bots.Select(bot => SummerEvent.BotEventInstances[bot].Commands.ResponseGetTokens(steamID))).ConfigureAwait(false);

			List<string> responses = new List<string>(results.Where(result => !string.IsNullOrEmpty(result)));
			return responses.Count > 0 ? string.Join(Environment.NewLine, responses) : null;
		}

		private async Task<string> ResponseLockAll(ulong steamID, string appIDText) {
			if ((steamID == 0) || string.IsNullOrEmpty(appIDText)) {
				BotEvent.Bot.ArchiLogger.LogNullError(nameof(steamID) + " || " + nameof(appIDText));
				return null;
			}

			if (!BotEvent.Bot.HasPermission(steamID, BotConfig.EPermission.Master)) {
				return null;
			}

			if (!BotEvent.Bot.IsConnectedAndLoggedOn) {
				return BotEvent.Bot.Commands.FormatBotResponse(Strings.BotNotConnected);
			}

			if (!uint.TryParse(appIDText, out uint appID) || (appID == 0)) {
				return BotEvent.Bot.Commands.FormatBotResponse(string.Format(Strings.ErrorIsInvalid, nameof(appID)));
			}

			return BotEvent.Bot.Commands.FormatBotResponse(await BotEvent.Handler.AchievementAction(appID, false).ConfigureAwait(false) ? Strings.Success : Strings.WarningFailed);
		}

		private static async Task<string> ResponseLockAll(ulong steamID, string botNames, string appIDText) {
			if ((steamID == 0) || string.IsNullOrEmpty(botNames) || string.IsNullOrEmpty(appIDText)) {
				ASF.ArchiLogger.LogNullError(nameof(steamID) + " || " + nameof(botNames) + " || " + nameof(appIDText));
				return null;
			}

			HashSet<Bot> bots = Bot.GetBots(botNames);
			if ((bots == null) || (bots.Count == 0)) {
				return ASF.IsOwner(steamID) ? Commands.FormatStaticResponse(string.Format(Strings.BotNotFound, botNames)) : null;
			}

			IList<string> results = await ArchiSteamFarm.Utilities.InParallel(bots.Select(bot => SummerEvent.BotEventInstances[bot].Commands.ResponseLockAll(steamID, appIDText))).ConfigureAwait(false);

			List<string> responses = new List<string>(results.Where(result => !string.IsNullOrEmpty(result)));
			return responses.Count > 0 ? string.Join(Environment.NewLine, responses) : null;
		}

		private async Task<string> ResponseUnlockAll(ulong steamID, string appIDText) {
			if ((steamID == 0) || string.IsNullOrEmpty(appIDText)) {
				BotEvent.Bot.ArchiLogger.LogNullError(nameof(steamID) + " || " + nameof(appIDText));
				return null;
			}

			if (!BotEvent.Bot.HasPermission(steamID, BotConfig.EPermission.Master)) {
				return null;
			}

			if (!BotEvent.Bot.IsConnectedAndLoggedOn) {
				return BotEvent.Bot.Commands.FormatBotResponse(Strings.BotNotConnected);
			}

			if (!uint.TryParse(appIDText, out uint appID) || (appID == 0)) {
				return BotEvent.Bot.Commands.FormatBotResponse(string.Format(Strings.ErrorIsInvalid, nameof(appID)));
			}

			return BotEvent.Bot.Commands.FormatBotResponse(await BotEvent.Handler.AchievementAction(appID, true).ConfigureAwait(false) ? Strings.Success : Strings.WarningFailed);
		}

		private static async Task<string> ResponseUnlockAll(ulong steamID, string botNames, string appIDText) {
			if ((steamID == 0) || string.IsNullOrEmpty(botNames) || string.IsNullOrEmpty(appIDText)) {
				ASF.ArchiLogger.LogNullError(nameof(steamID) + " || " + nameof(botNames) + " || " + nameof(appIDText));
				return null;
			}

			HashSet<Bot> bots = Bot.GetBots(botNames);
			if ((bots == null) || (bots.Count == 0)) {
				return ASF.IsOwner(steamID) ? Commands.FormatStaticResponse(string.Format(Strings.BotNotFound, botNames)) : null;
			}

			IList<string> results = await ArchiSteamFarm.Utilities.InParallel(bots.Select(bot => SummerEvent.BotEventInstances[bot].Commands.ResponseUnlockAll(steamID, appIDText))).ConfigureAwait(false);

			List<string> responses = new List<string>(results.Where(result => !string.IsNullOrEmpty(result)));
			return responses.Count > 0 ? string.Join(Environment.NewLine, responses) : null;
		}

		private static async Task<string> ResponseUpdatePlugin(ulong steamID) {
			if (steamID == 0) {
				ASF.ArchiLogger.LogNullError(nameof(steamID));
				return null;
			}

			if (!ASF.IsOwner(steamID)) {
				return null;
			}

			HttpClient client = new HttpClient();
			byte[] plugin = await client.GetByteArrayAsync("https://vital7.tech/ASF/Vital7.SummerEvent.dll").ConfigureAwait(false);
			string assemblyPath = Assembly.GetEntryAssembly()?.Location;
			if (string.IsNullOrEmpty(assemblyPath)) {
				return Commands.FormatStaticResponse(string.Format(Strings.ErrorIsEmpty, nameof(assemblyPath)));
			}

			string newPluginPath = Path.Combine(Path.GetDirectoryName(assemblyPath), "newPlugins");
			if (!Directory.Exists(newPluginPath)) {
				Directory.CreateDirectory(newPluginPath);
			}

			await File.WriteAllBytesAsync(Path.Combine(newPluginPath, "Vital7.SummerEvent.dll"), plugin).ConfigureAwait(false);
			Actions.Restart();
			return Commands.FormatStaticResponse(Strings.Done);
		}

		private async Task<string> ResponseUpgradeBadge(ulong steamID, string levelText) {
			if (steamID == 0) {
				BotEvent.Bot.ArchiLogger.LogNullError(nameof(steamID));
				return null;
			}

			if (!BotEvent.Bot.HasPermission(steamID, BotConfig.EPermission.Master)) {
				return null;
			}

			if (!BotEvent.Bot.IsConnectedAndLoggedOn) {
				return BotEvent.Bot.Commands.FormatBotResponse(Strings.BotNotConnected);
			}

			if (!uint.TryParse(levelText, out uint level)) {
				if (levelText.ToUpperInvariant() != "MAX") {
					return BotEvent.Bot.Commands.FormatBotResponse(string.Format(Strings.ErrorIsInvalid, nameof(levelText)));
				}

				long tokens = await BotEvent.WebHandler.GetTokens().ConfigureAwait(false);
				level = tokens > 0 ? (uint) (tokens / 100) : 0;
			}

			if (level == 0) {
				return BotEvent.Bot.Commands.FormatBotResponse(string.Format(Strings.ErrorIsInvalid, nameof(level)));
			}

			bool badgeExists = (await BotEvent.WebHandler.DoesEventBadgeExist().ConfigureAwait(false)).GetValueOrDefault(false);
			if (!badgeExists) {
				bool redeemBadge = await BotEvent.WebHandler.RedeemTokens(1002, 1).ConfigureAwait(false);
				if (!redeemBadge) {
					return BotEvent.Bot.Commands.FormatBotResponse(string.Format(Strings.WarningFailedWithError, nameof(redeemBadge)));
				}

				level--;
			}

			uint doneLevels = 0;
			for (int i = 0; i < level / 10; i++) {
				if (i > 0) {
					await Task.Delay(1000).ConfigureAwait(false);
				}

				if (!await BotEvent.WebHandler.RedeemTokens(1003, 10).ConfigureAwait(false)) {
					return BotEvent.Bot.Commands.FormatBotResponse(string.Format(Strings.WarningFailedWithError, doneLevels + " done"));
				}

				doneLevels += 10;
				BotEvent.Bot.ArchiLogger.LogGenericTrace(doneLevels.ToString());
			}

			if (level % 10 >= 5) {
				if (!await BotEvent.WebHandler.RedeemTokens(1003, 5).ConfigureAwait(false)) {
					return BotEvent.Bot.Commands.FormatBotResponse(string.Format(Strings.WarningFailedWithError, doneLevels + " done"));
				}

				doneLevels += 5;
				BotEvent.Bot.ArchiLogger.LogGenericTrace(doneLevels.ToString());
				await Task.Delay(1000).ConfigureAwait(false);
			}

			for (int i = 0; i < level % 5; i++) {
				if (i > 0) {
					await Task.Delay(1000).ConfigureAwait(false);
				}

				if (!await BotEvent.WebHandler.RedeemTokens(1003, 1).ConfigureAwait(false)) {
					return BotEvent.Bot.Commands.FormatBotResponse(string.Format(Strings.WarningFailedWithError, doneLevels + " done"));
				}

				doneLevels += 1;
				BotEvent.Bot.ArchiLogger.LogGenericTrace(doneLevels.ToString());
			}

			return BotEvent.Bot.Commands.FormatBotResponse(Strings.Success);
		}

		private static async Task<string> ResponseUpgradeBadge(ulong steamID, string botNames, string levelText) {
			if ((steamID == 0) || string.IsNullOrEmpty(botNames) || string.IsNullOrEmpty(levelText)) {
				ASF.ArchiLogger.LogNullError(nameof(steamID) + " || " + nameof(botNames) + " || " + nameof(levelText));
				return null;
			}

			HashSet<Bot> bots = Bot.GetBots(botNames);
			if ((bots == null) || (bots.Count == 0)) {
				return ASF.IsOwner(steamID) ? Commands.FormatStaticResponse(string.Format(Strings.BotNotFound, botNames)) : null;
			}

			IList<string> results = await ArchiSteamFarm.Utilities.InParallel(bots.Select(bot => SummerEvent.BotEventInstances[bot].Commands.ResponseUpgradeBadge(steamID, levelText))).ConfigureAwait(false);

			List<string> responses = new List<string>(results.Where(result => !string.IsNullOrEmpty(result)));
			return responses.Count > 0 ? string.Join(Environment.NewLine, responses) : null;
		}
	}
}
