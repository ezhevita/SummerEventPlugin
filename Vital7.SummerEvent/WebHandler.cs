using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using ArchiSteamFarm;
using ArchiSteamFarm.Json;
using ArchiSteamFarm.Localization;
using HtmlAgilityPack;
using Newtonsoft.Json;
using SteamKit2;

namespace Vital7.SummerEvent {
	internal class WebHandler {
		private const string SteamCommunityURL = "https://steamcommunity.com";
		private const string SteamStoreURL = "https://store.steampowered.com";
		private readonly Bot Bot;

		internal WebHandler(Bot bot) => Bot = bot;

		internal async Task<bool?> AddToWishlist(uint appID) {
			const string requestWishlistAdd = "/api/addtowishlist";
			string responseWishlistAdd = (await Bot.ArchiWebHandler.UrlPostToHtmlDocumentWithSession(SteamStoreURL, requestWishlistAdd, new Dictionary<string, string>(2, StringComparer.Ordinal) {
				{"appid", appID.ToString()}
			}).ConfigureAwait(false))?.ParsedText;
			if (string.IsNullOrEmpty(responseWishlistAdd)) {
				Bot.ArchiLogger.LogGenericWarning(string.Format(Strings.ErrorIsEmpty, nameof(responseWishlistAdd)));
				return null;
			}

			try {
				dynamic wishlistAddResponse = JsonConvert.DeserializeObject(responseWishlistAdd);
				return (bool) wishlistAddResponse.success;
			} catch (JsonException ex) {
				Bot.ArchiLogger.LogGenericWarningException(ex);
				return null;
			}
		}

		internal async Task<bool?> ClaimPastAchievements(uint appID) {
			if (appID == 0) {
				Bot.ArchiLogger.LogNullError(nameof(appID));
				return false;
			}

			Steam.NumberResponse response = await Bot.ArchiWebHandler.UrlPostToJsonObjectWithSession<Steam.NumberResponse>(SteamStoreURL, "/grandprix/ajaxclaimpointsforpastachievements/", new Dictionary<string, string>(2) {
				{"appid", appID.ToString()}
			}).ConfigureAwait(false);

			return response?.Success;
		}

		internal async Task<bool?> DoesEventBadgeExist() {
			HtmlDocument badgePage = await Bot.ArchiWebHandler.UrlGetToHtmlDocumentWithSession(SteamCommunityURL, "/my/badges/37?l=en", false).ConfigureAwait(false);
			if (badgePage == null) {
				return null;
			}

			return badgePage.DocumentNode.SelectSingleNode("//div[@class='badge_row depressed']") != null;
		}

		internal async Task<GrandPrixPageInfo> GetGrandPrixInfo() {
			HtmlDocument grandPrixPage = await Bot.ArchiWebHandler.UrlGetToHtmlDocumentWithSession(SteamStoreURL, "/grandprix?l=en").ConfigureAwait(false);
			if (grandPrixPage == null) {
				return null;
			}

			GrandPrixPageInfo info = new GrandPrixPageInfo {
				AvailableTasks = grandPrixPage.DocumentNode.SelectNodes("//div[@class='prix_clipboard_field_box']")?.Select(node => node.ChildNodes.All(childNode => childNode.Name != "img")).ToArray()
			};

			string capacityLine = grandPrixPage.Text.Split('\n').FirstOrDefault(line => line.TrimStart().StartsWith("g_nBoostBankCapacity", StringComparison.InvariantCulture));
			if (!string.IsNullOrEmpty(capacityLine) && uint.TryParse(capacityLine.Split('=').LastOrDefault()?.Trim('\t', '\r', '\n', ' ', ';'), out uint capacity)) {
				info.Capacity = capacity;
			} else {
				Bot.ArchiLogger.LogGenericDebug(nameof(capacityLine) + ":" + capacityLine);
			}

			string pointsLine = grandPrixPage.Text.Split('\n').FirstOrDefault(line => line.TrimStart().StartsWith("g_nCurrentPoints", StringComparison.InvariantCulture));
			if (!string.IsNullOrEmpty(pointsLine) && uint.TryParse(pointsLine.Split('=').LastOrDefault()?.Trim('\t', '\r', '\n', ' ', ';'), out uint points)) {
				info.Points = points;
			} else {
				Bot.ArchiLogger.LogGenericDebug(nameof(pointsLine) + ":" + capacityLine);
			}

			string appsLine = grandPrixPage.Text.Split('\n').FirstOrDefault(line => line.TrimStart().StartsWith("var rgAppsWithPointsFromPastAchievements", StringComparison.InvariantCulture));
			if (!string.IsNullOrEmpty(appsLine)) {
				string appsText = appsLine.Split('=').LastOrDefault()?.Trim('\t', '\r', '\n', ' ', ';').Trim();
				if (!string.IsNullOrEmpty(appsText)) {
					try {
						info.PastGames = JsonConvert.DeserializeObject<HashSet<uint>>(appsText);
					} catch (JsonException ex) {
						Bot.ArchiLogger.LogGenericWarningException(ex);
					}
				} else {
					Bot.ArchiLogger.LogGenericWarning(string.Format(Strings.ErrorObjectIsNull, nameof(appsText)));
				}
			} else {
				Bot.ArchiLogger.LogGenericWarning(string.Format(Strings.ErrorObjectIsNull, nameof(appsLine)));
			}

			string currentTeamLine = grandPrixPage.Text.Split('\n').FirstOrDefault(line => line.TrimStart().StartsWith("var g_nCurrentTeam", StringComparison.InvariantCulture));
			if (!string.IsNullOrEmpty(currentTeamLine)) {
				string teamValueText = currentTeamLine.Split('=').LastOrDefault()?.Trim('\t', '\r', '\n', ' ', ';');
				if (!string.IsNullOrEmpty(teamValueText)) {
					info.CurrentTeam = byte.TryParse(teamValueText, out byte teamID) ? (byte?) teamID : null;
				} else {
					Bot.ArchiLogger.LogGenericWarning(string.Format(Strings.ErrorObjectIsNull, nameof(teamValueText)));
				}
			} else {
				Bot.ArchiLogger.LogGenericWarning(string.Format(Strings.ErrorObjectIsNull, nameof(currentTeamLine)));
			}

			string mapConsumablesLine = grandPrixPage.Text.Split('\n').FirstOrDefault(line => line.TrimStart().StartsWith("g_mapConsumables", StringComparison.InvariantCulture));
			if (!string.IsNullOrEmpty(mapConsumablesLine)) {
				string mapConsumablesText = mapConsumablesLine.Split('=').LastOrDefault()?.Trim('\t', '\r', '\n', ' ', ';').Trim();
				if (!string.IsNullOrEmpty(mapConsumablesText)) {
					Dictionary<uint, dynamic> mapConsumables = null;
					try {
						mapConsumables = JsonConvert.DeserializeObject<Dictionary<uint, object>>(mapConsumablesText);
					} catch (JsonException ex) {
						Bot.ArchiLogger.LogGenericWarningException(ex);
					}

					if (mapConsumables != null) {
						if (!mapConsumables.ContainsKey(5)) {
							info.IsSwitchTeamAvailable = false;
						} else {
							uint timestamp = (uint) mapConsumables[5].timestamp_expiration;
							info.IsSwitchTeamAvailable = timestamp > DateTimeOffset.UtcNow.ToUnixTimeSeconds();
						}
					}
				} else {
					Bot.ArchiLogger.LogGenericWarning(string.Format(Strings.ErrorObjectIsNull, nameof(mapConsumablesLine)));
				}
			} else {
				Bot.ArchiLogger.LogGenericWarning(string.Format(Strings.ErrorObjectIsNull, nameof(mapConsumablesLine)));
			}

			return info;
		}

		internal async Task<ulong?> GetLevel() {
			HtmlDocument badgePage = await Bot.ArchiWebHandler.UrlGetToHtmlDocumentWithSession(SteamCommunityURL, "/my/badges/37?l=en", false).ConfigureAwait(false);
			if (badgePage == null) {
				return null;
			}

			string xpText = badgePage.DocumentNode.SelectSingleNode("//div[@class='badge_info_description']/div[2]").InnerText.Trim().Split(' ')[0].Replace(",", "");
			if (!ulong.TryParse(xpText, out ulong xp) || (xp == 0)) {
				Bot.ArchiLogger.LogGenericWarning(string.Format(Strings.ErrorIsInvalid, nameof(xp)));
				return null;
			}

			return xp / 100;
		}

		internal async Task<bool?> JoinTeam(byte teamID) {
			if (teamID == 0) {
				Bot.ArchiLogger.LogNullError(nameof(teamID));
				return null;
			}

			Steam.NumberResponse joinTeamResponse = await Bot.ArchiWebHandler.UrlPostToJsonObjectWithSession<Steam.NumberResponse>(SteamStoreURL, "/grandprix/ajaxjointeam/", new Dictionary<string, string>(2, StringComparer.Ordinal) {
				{"teamid", teamID.ToString()}
			}).ConfigureAwait(false);

			return joinTeamResponse?.Success;
		}

		internal async Task<Dictionary<uint, uint>> GetPlaytime() {
			if ((Bot.SteamID == 0) || !new SteamID(Bot.SteamID).IsIndividualAccount) {
				Bot.ArchiLogger.LogNullError(nameof(Bot.SteamID));
				return null;
			}

			(bool success, string steamApiKey) = await Bot.ArchiWebHandler.CachedApiKey.GetValue().ConfigureAwait(false);
			if (!success || string.IsNullOrEmpty(steamApiKey)) {
				return null;
			}

			KeyValue response = null;
			for (byte i = 0; (i < 5) && (response == null); i++) {
				using (WebAPI.AsyncInterface iPlayerService = Bot.SteamConfiguration.GetAsyncWebAPIInterface("IPlayerService")) {
					iPlayerService.Timeout = Bot.ArchiWebHandler.WebBrowser.Timeout;

					try {
						response = await ArchiWebHandler.WebLimitRequest(
							WebAPI.DefaultBaseAddress.Host,
							// ReSharper disable once AccessToDisposedClosure
							async () => await iPlayerService.CallAsync(
								HttpMethod.Get, "GetOwnedGames", args: new Dictionary<string, object>(2, StringComparer.Ordinal) {
									{"key", steamApiKey},
									{"steamid", Bot.SteamID}
								}
							).ConfigureAwait(false)
						).ConfigureAwait(false);
					} catch (TaskCanceledException e) {
						Bot.ArchiLogger.LogGenericDebuggingException(e);
					} catch (Exception e) {
						Bot.ArchiLogger.LogGenericWarningException(e);
					}
				}
			}

			if (response == null) {
				Bot.ArchiLogger.LogGenericWarning(string.Format(Strings.ErrorRequestFailedTooManyTimes, 5));
				return null;
			}

			List<KeyValue> games = response["games"].Children;
			Dictionary<uint, uint> result = new Dictionary<uint, uint>(games.Count);
			foreach (KeyValue game in games) {
				uint appID = game["appid"].AsUnsignedInteger();
				if (appID == 0) {
					Bot.ArchiLogger.LogNullError(nameof(appID));
					return null;
				}

				uint playtime = game["playtime_forever"].AsUnsignedInteger();
				result.Add(appID, playtime);
			}

			return result;
		}

		internal async Task<uint?> GetRandomGame() {
			const string request = "https://store.steampowered.com/search/results?sort_by=Price_DESC&tags=-1&category1=998&hide_filtered_results_warning=1&page=";
			HtmlDocument gamePage = (await Bot.ArchiWebHandler.WebBrowser.UrlGetToHtmlDocument(request + (Utilities.RandomNext(50) + 1)).ConfigureAwait(false))?.Content;
			if (gamePage == null) {
				return null;
			}

			HtmlNodeCollection nodes = gamePage.DocumentNode.SelectNodes("//a[starts-with(@class, 'search_result_row ds_collapse_flag')]");
			if ((nodes == null) || (nodes.Count == 0)) {
				Bot.ArchiLogger.LogGenericWarning(string.Format(Strings.ErrorIsInvalid, nameof(nodes)));
				return null;
			}

			uint appID = (uint) nodes.GetRandomValue().GetAttributeValue("data-ds-appid", 0);
			if (appID == 0) {
				Bot.ArchiLogger.LogGenericWarning(string.Format(Strings.ErrorParsingObject, nameof(appID)));
				return null;
			}

			return appID;
		}

		internal async Task<uint?> GetRandomFreeGame() {
			const string request = "https://store.steampowered.com/search/results?sort_by=Released_DESC&genre=Free%20to%20Play&tags=-1&category1=998&hide_filtered_results_warning=1&page=";
			HtmlDocument gamePage = (await Bot.ArchiWebHandler.WebBrowser.UrlGetToHtmlDocument(request + (Utilities.RandomNext(50) + 1)).ConfigureAwait(false))?.Content;
			if (gamePage == null) {
				return null;
			}

			HtmlNodeCollection nodes = gamePage.DocumentNode.SelectNodes("//a[starts-with(@class, 'search_result_row ds_collapse_flag')]");
			if ((nodes == null) || (nodes.Count == 0)) {
				Bot.ArchiLogger.LogGenericWarning(string.Format(Strings.ErrorIsInvalid, nameof(nodes)));
				return null;
			}

			uint appID = (uint) nodes.GetRandomValue().GetAttributeValue("data-ds-appid", 0);
			if (appID == 0) {
				Bot.ArchiLogger.LogGenericWarning(string.Format(Strings.ErrorParsingObject, nameof(appID)));
				return null;
			}

			return appID;
		}

		internal async Task<long> GetTokens() {
			HtmlDocument pitstopPage = await Bot.ArchiWebHandler.UrlGetToHtmlDocumentWithSession(SteamStoreURL, "/pitstop?l=en").ConfigureAwait(false);
			if (pitstopPage == null) {
				return 0;
			}

			try {
				return long.Parse(pitstopPage.DocumentNode.SelectSingleNode("//div[@class='rewards_num_points']")?.InnerText.Replace(",", ""));
			} catch (Exception e) {
				Bot.ArchiLogger.LogGenericWarningException(e);
				return 0;
			}
		}

		internal async Task<bool> SwitchTeam(uint currentTeam) {
			Steam.NumberResponse response = await Bot.ArchiWebHandler.UrlPostToJsonObjectWithSession<Steam.NumberResponse>(SteamStoreURL, "/grandprix/ajaxuseconsumable", new Dictionary<string, string>(3) {
				{"teamid", currentTeam.ToString()},
				{"consumable", "5"}
			}).ConfigureAwait(false);

			return response.Success;
		}

		internal async Task<bool> RedeemTokens(ushort itemID, byte level) {
			if ((itemID == 0) || (level == 0)) {
				Bot.ArchiLogger.LogNullError(nameof(itemID) + " || " + nameof(level));
				return false;
			}

			Steam.NumberResponse responseBadge = await Bot.ArchiWebHandler.UrlPostToJsonObjectWithSession<Steam.NumberResponse>(SteamStoreURL, "/pitstop/ajaxredeemtokens/", new Dictionary<string, string>(3, StringComparer.Ordinal) {
				{"itemid", itemID.ToString()},
				{"num_badge_levels", level.ToString()}
			}).ConfigureAwait(false);

			return responseBadge?.Success == true;
		}

		internal class GrandPrixPageInfo {
			internal bool[] AvailableTasks;
			internal uint? Capacity;
			internal byte? CurrentTeam;
			internal bool? IsSwitchTeamAvailable;
			internal HashSet<uint> PastGames;
			internal uint? Points;
		}
	}
}
