﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Composition;
using System.Threading.Tasks;
using ArchiSteamFarm;
using ArchiSteamFarm.Plugins;
using Newtonsoft.Json.Linq;
using SteamKit2;

namespace Vital7.SummerEvent {
	[Export(typeof(IPlugin))]
	// ReSharper disable once ClassNeverInstantiated.Global
	internal sealed class SummerEvent : IBot, IBotCommand, IBotConnection, IBotModules, IBotSteamClient, IASF {
		internal static ConcurrentDictionary<Bot, BotEventInstance> BotEventInstances;
		internal static DateTime SummerEventEnd = new DateTime(2019, 07, 07, 17, 00, 00);

		public void OnASFInit(IReadOnlyDictionary<string, JToken> additionalConfigProperties) {
			BotEventInstances = new ConcurrentDictionary<Bot, BotEventInstance>();
		}

		public void OnBotDestroy(Bot bot) {
			if (!BotEventInstances.ContainsKey(bot)) {
				ASF.ArchiLogger.LogNullError(nameof(bot));
				return;
			}

			BotEventInstances[bot].Dispose();
			BotEventInstances.TryRemove(bot, out _);
		}

		public void OnBotInit(Bot bot) {
			BotEventInstances.TryAdd(bot, new BotEventInstance(bot));
		}

		public string Name => nameof(SummerEvent);
		public Version Version => PluginVersion;

		internal static readonly Version PluginVersion = new Version(2, 2, 1, 1);
		
		public void OnLoaded() {
			ASF.ArchiLogger.LogGenericInfo(Name + " is initialized successfully!");
		}

		public async Task<string> OnBotCommand(Bot bot, ulong steamID, string message, string[] args) {
			if (!BotEventInstances.ContainsKey(bot)) {
				ASF.ArchiLogger.LogNullError(nameof(bot));
				return null;
			}

			return await BotEventInstances[bot].OnBotCommand(steamID, message, args).ConfigureAwait(false);
		}

		public void OnBotDisconnected(Bot bot, EResult reason) {
			if (!BotEventInstances.ContainsKey(bot)) {
				ASF.ArchiLogger.LogNullError(nameof(bot));
				return;
			}

			BotEventInstances[bot].OnBotDisconnected();
		}

		public void OnBotLoggedOn(Bot bot) {
			if (!BotEventInstances.ContainsKey(bot)) {
				ASF.ArchiLogger.LogNullError(nameof(bot));
				return;
			}

			BotEventInstances[bot].OnBotLoggedOn();
		}

		public void OnBotInitModules(Bot bot, IReadOnlyDictionary<string, JToken> additionalConfigProperties = null) {
			if (!BotEventInstances.ContainsKey(bot)) {
				ASF.ArchiLogger.LogNullError(nameof(bot));
				return;
			}

			if (additionalConfigProperties == null) {
				return;
			}

			BotEventInstances[bot].OnBotInitModules(additionalConfigProperties);
		}

		public void OnBotSteamCallbacksInit(Bot bot, CallbackManager callbackManager) {
		}

		public IReadOnlyCollection<ClientMsgHandler> OnBotSteamHandlersInit(Bot bot) {
			if (!BotEventInstances.ContainsKey(bot)) {
				ASF.ArchiLogger.LogNullError(nameof(bot));
				return null;
			}

			return new ClientMsgHandler[] {BotEventInstances[bot].Handler};
		}
	}
}
