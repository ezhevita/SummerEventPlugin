using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using ArchiSteamFarm;

namespace Vital7.SummerEvent {
	internal static class PrivateMembersExtensions {
		internal static Task<Dictionary<uint, string>> FetchGamesOwned(this Commands commands, bool cachedOnly = false) => (Task<Dictionary<uint, string>>) typeof(Commands).GetMethod("FetchGamesOwned", BindingFlags.Instance | BindingFlags.NonPublic).Invoke(commands, new object[] {cachedOnly});

		internal static Task<bool> DoTaskBroadcast(this ArchiWebHandler archiWebHandler) => (Task<bool>) typeof(ArchiWebHandler).GetMethod("DoTaskBroadcast", BindingFlags.Instance | BindingFlags.NonPublic).Invoke(archiWebHandler, new object[0]);
	}
}
