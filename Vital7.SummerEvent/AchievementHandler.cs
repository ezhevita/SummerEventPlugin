using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using ArchiSteamFarm;
using ArchiSteamFarm.Localization;
using ArchiSteamFarm.NLog;
using SteamKit2;
using SteamKit2.Internal;

namespace Vital7.SummerEvent {
	internal class AchievementHandler : ClientMsgHandler {
		private readonly ArchiLogger ArchiLogger;

		internal AchievementHandler(Bot bot) {
			if (bot == null) {
				ASF.ArchiLogger.LogNullError(nameof(bot));
				return;
			}

			ArchiLogger = bot.ArchiLogger;
		}

		public override void HandleMsg(IPacketMsg packetMsg) {
			if (packetMsg == null) {
				ArchiLogger.LogNullError(nameof(packetMsg));
				return;
			}

			switch (packetMsg.MsgType) {
				case EMsg.ClientGetUserStatsResponse: {
					ClientMsgProtobuf<CMsgClientGetUserStatsResponse> response = new ClientMsgProtobuf<CMsgClientGetUserStatsResponse>(packetMsg);
					Client.PostCallback(new GetUserStatsCallback(packetMsg.TargetJobID, response.Body));
					break;
				}

				case EMsg.ClientStoreUserStatsResponse: {
					ClientMsgProtobuf<CMsgClientStoreUserStatsResponse> response = new ClientMsgProtobuf<CMsgClientStoreUserStatsResponse>(packetMsg);
					Client.PostCallback(new StoreUserStatsCallback(packetMsg.TargetJobID, response.Body));
					break;
				}
			}
		}

		internal async Task<bool> AchievementAction(uint appID, bool unlock) {
			if (appID == 0) {
				ArchiLogger.LogNullError(nameof(appID));
				return false;
			}

			if (Client == null) {
				ArchiLogger.LogNullError(nameof(Client));
				return false;
			}

			if (!Client.IsConnected) {
				return false;
			}

			try {
				ClientMsgProtobuf<CMsgClientGetUserStats> getUserStatsRequest = new ClientMsgProtobuf<CMsgClientGetUserStats>(EMsg.ClientGetUserStats) {
					Body = {
						game_id = appID,
						schema_local_version = -1,
						steam_id_for_user = Client.SteamID.ConvertToUInt64()
					},
					SourceJobID = Client.GetNextJobID()
				};

				Client.Send(getUserStatsRequest);
				GetUserStatsCallback getUserStatsResponse = await new AsyncJob<GetUserStatsCallback>(Client, getUserStatsRequest.SourceJobID);
				if (getUserStatsResponse == null) {
					ArchiLogger.LogGenericWarning(string.Format(Strings.ErrorIsEmpty, nameof(getUserStatsResponse)));
					return false;
				}

				if (getUserStatsResponse.Result != EResult.OK) {
					ArchiLogger.LogGenericWarning(string.Format(Strings.WarningFailedWithError, nameof(getUserStatsRequest) + "/" + getUserStatsResponse.Result));
					return false;
				}

				List<CMsgClientStoreUserStats2.Stats> stats = new List<CMsgClientStoreUserStats2.Stats>();

				foreach (KeyValue child in getUserStatsResponse.Schema["stats"].Children) {
					if (child["bits"] == KeyValue.Invalid) {
						continue;
					}

					if (unlock) {
						uint sum = 0;
						foreach (KeyValue bitChild in child["bits"].Children) {
							sum += (uint) Math.Pow(2, byte.Parse(bitChild.Name));
						}

						CMsgClientStoreUserStats2.Stats childStats = new CMsgClientStoreUserStats2.Stats {
							stat_id = uint.Parse(child.Name),
							stat_value = sum
						};

						stats.Add(childStats);
					} else {
						stats.Add(new CMsgClientStoreUserStats2.Stats {
							stat_id = uint.Parse(child.Name),
							stat_value = 0
						});
					}
				}

				ClientMsgProtobuf<CMsgClientStoreUserStats2> storeUserStatsRequest = new ClientMsgProtobuf<CMsgClientStoreUserStats2>(EMsg.ClientStoreUserStats2) {
					Body = {
						crc_stats = getUserStatsResponse.CRCStats,
						explicit_reset = false,
						game_id = appID,
						settor_steam_id = Client.SteamID,
						settee_steam_id = Client.SteamID,
						stats = {
							Capacity = stats.Count
						}
					},
					SourceJobID = Client.GetNextJobID()
				};

				foreach (CMsgClientStoreUserStats2.Stats stat in stats) {
					storeUserStatsRequest.Body.stats.Add(stat);
				}

				Client.Send(storeUserStatsRequest);
				StoreUserStatsCallback storeUserStatsResponse = await new AsyncJob<StoreUserStatsCallback>(Client, storeUserStatsRequest.SourceJobID);
				if (storeUserStatsResponse == null) {
					ArchiLogger.LogGenericWarning(string.Format(Strings.ErrorIsEmpty, nameof(getUserStatsResponse)));
					return false;
				}

				if (storeUserStatsResponse.Result != EResult.OK) {
					ArchiLogger.LogGenericWarning(string.Format(Strings.WarningFailedWithError, nameof(storeUserStatsResponse) + "/" + storeUserStatsResponse.Result));
					return false;
				}

				return true;
			} catch (TaskCanceledException e) {
				ArchiLogger.LogGenericWarningException(e);
				return false;
			}
		}

		private sealed class GetUserStatsCallback : CallbackMsg {
			internal readonly uint CRCStats;
			internal readonly EResult Result;
			internal readonly KeyValue Schema;

			internal GetUserStatsCallback(JobID jobID, CMsgClientGetUserStatsResponse msg) {
				if ((jobID == null) || (msg == null)) {
					throw new ArgumentNullException(nameof(jobID) + " || " + nameof(msg));
				}

				JobID = jobID;
				CRCStats = msg.crc_stats;
				Result = (EResult) msg.eresult;
				using (MemoryStream stream = new MemoryStream(msg.schema)) {
					KeyValue keyValue = new KeyValue();
					keyValue.TryReadAsBinary(stream);
					Schema = keyValue;
				}
			}
		}

		private sealed class StoreUserStatsCallback : CallbackMsg {
			internal readonly EResult Result;

			internal StoreUserStatsCallback(JobID jobID, CMsgClientStoreUserStatsResponse msg) {
				if ((jobID == null) || (msg == null)) {
					throw new ArgumentNullException(nameof(jobID) + " || " + nameof(msg));
				}

				JobID = jobID;
				Result = (EResult) msg.eresult;
			}
		}
	}
}
