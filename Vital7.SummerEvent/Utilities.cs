using System;
using System.Collections.Generic;
using System.Linq;
using ArchiSteamFarm;

namespace Vital7.SummerEvent {
	internal static class Utilities {
		private static readonly Random Random = new Random();

		internal static int RandomNext(int maxWithout) {
			if (maxWithout <= 0) {
				ASF.ArchiLogger.LogNullError(nameof(maxWithout));
				return -1;
			}

			if (maxWithout == 1) {
				return 0;
			}

			lock (Random) {
				return Random.Next(maxWithout);
			}
		}

		internal static T GetRandomValue<T>(this ICollection<T> set) {
			if ((set == null) || (set.Count == 0)) {
				ASF.ArchiLogger.LogNullError(nameof(set) + " || " + nameof(set.Count));
				return default;
			}

			return set.ElementAt(RandomNext(set.Count));
		}
	}
}
